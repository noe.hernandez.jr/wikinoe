package lambda.basic;

	public class LambdaExample {

		public static void main(String[] args) {
			Thread thread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					System.out.println("Inside Runnable");
				}
			});
			
			// spawn a new Thread
			thread.run();
			
			Thread myLambdaThread = new Thread(() -> System.out.println("Lambda Runnable"));
			myLambdaThread.run();
		}
	}


