# Noah's Wiki Page
**Software Developer/Engineer**

*What makes me unique?*
1. Bachelor of Science Degree in Software Development
2. Granted Summa Cum Laude graduation status
3. Lightning-fast learner
4. Exceptional perseverance and determination
5. Outstanding professionalism
6. Extremely diligent
7. Bilingual (Fluent in Spanish as a native speaker)
8. Amicable and a team player

| Skills | Proficiency | 
| ------ | ----------- |
| Java | Intermediate |
| C++ | Intermediate |
| Object-Oriented Programming | Intermediate |
| Data Structures & Algorithms | Intermediate |
| User-Experience| Beginner |
| Database Management | Beginner |
| Mobile Device Programming | Beginner |
| AI and Machine Learning | Beginner |
| Web Development | Beginner |

==AVAILABLE FOR WORK IMMEDIATELY==